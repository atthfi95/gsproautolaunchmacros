# GSProAutoLaunchMacros

## Description
This script will automatically open GSPro golf simulator, place advanced launch addon on the RIGHT (secondary) monitor
and minimize unnecessary windows from LEFT (primary) monitor, allowing user to enter game mode with ease.

This script is supposed to run on startup, This can be achieved by placing powershell shortcut into the shell:startup directory.

## Prerequisities
This powershell script has following requirements

1. You have autohotkey installed
2. You have keystrokeMapper.ahk in your shell:startup folder and it succesfully starts during boot
3. You are connected to FlightScope
4. Primary monitor is on the LEFT side of the secondary monitor

## Work In Progress
- [x] Working navigation via GUI
- [ ] Play sound when game enviroment is ready for user input.
- [ ] Replace Start-Sleep delays with conditions (if window is up, navigate through it)

## Installation
This script is supposed to run on startup, This can be achieved by placing powershell shortcut into the shell:startup directory.

For prerequisities, see above.

## Authors
Author [Atte Hostikka](https://t.me/atthostt)

## License
     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 