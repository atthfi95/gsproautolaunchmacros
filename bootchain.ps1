# Startup delay
Start-Sleep 5
# TODO; add condition: "if in SSID (flightscope ssid)"
# 
# Start GSPro
# TODO; convert absolute paths to relative paths !
$app = "C:\GSProV1\Core\GSP\GSPro.exe"
Start-Process -FilePath $app
# Wait for process to start
# TODO; add condition "if GSPro Configuration windows exists, then ..."
Start-Sleep 9
$gsproapp = New-Object -ComObject wscript.shell;
Start-Sleep 1
# Select GSPro conf window and enter play
$gsproapp.AppActivate('GSPro Configuration')
$gsproapp.SendKeys('{ENTER}')
Start-Sleep 20
#
# Connect flightscope via gui
$fscopeapp = New-Object -ComObject wscript.shell;
# TODO; add condition
$fscopeapp.AppActivate('Flightscope v1.9.22')
# Navigate gui 1/6 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 2/6 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 3/6 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 4/6 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 5/6 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 6/6 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# enter connect button
$fscopeapp.SendKeys('{ENTER}')
Start-Sleep 8
#
# Start window arrangements
# TODO; add condition
$fscopeapp.AppActivate('Flightscope v1.9.22')
# Navigate gui 1/4 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 2/4 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 3/4 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# Navigate gui 4/4 tabs
$fscopeapp.SendKeys('{tab}')
Start-Sleep 0.1
# enter launch advanced
$fscopeapp.SendKeys('{ENTER}')
Start-Sleep 4
#
# minimize flightscope from main monitor
# select flightscope v1.9.22
# TODO; add condition
$fscopeapp.AppActivate('Flightscope v1.9.22')
Start-Sleep 0.1
# F12 mapped via AHK to minimize window
# TODO; add better solution to handle window arrangements
$fscopeapp.SendKeys('{F12}')
Start-Sleep 1
#
# select Advanced Connect Add On ikkuna
$connectaddonapp = New-Object -ComObject wscript.shell;
# TODO; add condition
$connectaddonapp.AppActivate('Advanced Connect Add On')
Start-Sleep 0.1
# F10 mapped via AHK to move window to RIGHT
$connectaddonapp.SendKeys('{F10}')
Start-Sleep 1
# escape awful windows native features with esc
$connectaddonapp.SendKeys('{esc}')
Start-Sleep 1
# F11 mapped via AHK to maximize window
$connectaddonapp.SendKeys('{F11}')
Start-Sleep 1
$connectaddonapp.SendKeys('{F11}')
Start-Sleep 1
